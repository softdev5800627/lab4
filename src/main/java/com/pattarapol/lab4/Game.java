/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.lab4;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Game {

    private Player player1, player2;
    private Table table;
    private static String isContinue;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');

    }

    public void play() {
        boolean isFinsh = false;
        printWelcome();
        newGame();
        while (!isFinsh) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()) {
                printTable();
                printWinner();
                printPlayers();
                isFinsh = true;
            }
            if (table.checkDraw()) {
                printTable();
                printDraw();
                printPlayers();
                isFinsh = true;
            }
            table.switchPlayer();
                    
        }
        inputContinue();
    }

    private void printWelcome() {
        System.out.println("Welcome to OX Game!!!!!");
    }

    private void printTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input row col:");
        int row = kb.nextInt();
        int col = kb.nextInt();
        table.setRowCol(row, col);

    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printWinner() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Win!!!!");
    }

    private void printDraw() {
        System.out.println(table.getCurrentPlayer() + " Draw!!");
    }
    
    private void printPlayers(){
        System.out.println(player1);
        System.out.println(player2);
        
        
    }
    
    private void  inputContinue(){
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Do you for playing again?(y/n) :");
            isContinue = kb.next();
            if(isContinue.equalsIgnoreCase("y")){
                newGame();
                play();
                break;
            }else if(isContinue.equalsIgnoreCase("n")){
                System.out.println("Goodbye!!!!");
                break;
            }
        }
    }
}
